This project borrows heavily from the excellent [Laravel 4](http://laravel.com) and [Symfony 2](http://symfony.com) projects.

## License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

## Standalone
~~~~
<?php
include 'vendors/autoload.php';
$loader = new \Smorken\Config\FileLoader(__DIR__); //__DIR__ represents base path where config files are stored
$config = new \Smorken\Config\Config($loader);
~~~~

## Part of simple app

Should automatically be included if using composer to set up the project

Otherwise add a service line to config/app.php services array

```
'Smorken\Config\ConfigService'
```