<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 9:11 AM
 */

namespace Smorken\Config;

use Smorken\Service\Service;
use Smorken\Utils\PathUtils;

class ConfigService extends Service {

    protected $deferred = false;

    public function start()
    {
        $this->name = 'config';
    }

    /**
     * Binds Config to 'config' in App
     * This instance takes FileLoader as a constructor argument
     * FileLoader is the handler for loading the config files from disk and
     * takes the base path to the config files as its constructor argument
     */
    public function load()
    {
        $this->app->instance('config', function($c) {
            $loader = new \Smorken\Config\FileLoader(PathUtils::base() . '/config');
            return new \Smorken\Config\Config($loader);
        });
    }
} 