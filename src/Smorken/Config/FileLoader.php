<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 10:16 AM
 */

namespace Smorken\Config;


class FileLoader implements LoaderInterface {

    protected $basepath;

    /**
     * Construct the FileLoader
     * @param string $basepath base path to the config files
     */
    public function __construct($basepath)
    {
        $this->basepath = $basepath;
    }

    /**
     * Loads the file specified by $group
     * @param $group
     * @return array|mixed
     */
    public function load($group)
    {
        $items = array();

        $file = "{$this->basepath}/{$group}.php";

        if (file_exists($file)) {
            $items = require $file;
        }
        return $items;
    }
}