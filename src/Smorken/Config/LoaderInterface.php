<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 10:15 AM
 */

namespace Smorken\Config;


interface LoaderInterface {

    /**
     * Loads the requested config
     * @param $group
     * @return mixed
     */
    public function load($group);
}