<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 7:31 AM
 */

use Smorken\Config\Config;
use Mockery as m;

class ConfigTest extends \PHPUnit_Framework_TestCase {

    public function testHasReturnsTrueIfExists()
    {
        $l = m::mock('Smorken\Config\LoaderInterface');
        $l->shouldReceive('load')
            ->once()
            ->with('group')
            ->andReturn($this->getConfigItems());
        $config = new Config($l);
        $this->assertTrue($config->has('group.foo'));
    }

    public function testHasReturnsFalseIfNotExists()
    {
        $l = m::mock('Smorken\Config\LoaderInterface');
        $l->shouldReceive('load')
            ->once()
            ->with('group')
            ->andReturn($this->getConfigItems());
        $config = new Config($l);
        $this->assertFalse($config->has('group.no'));
    }

    public function testGetReturnsValueIfExists()
    {
        $l = m::mock('Smorken\Config\LoaderInterface');
        $l->shouldReceive('load')
            ->once()
            ->with('group')
            ->andReturn($this->getConfigItems());
        $config = new Config($l);
        $this->assertEquals('bar', $config->get('group.foo'));
    }

    public function testGetEntireConfig()
    {
        $l = m::mock('Smorken\Config\LoaderInterface');
        $l->shouldReceive('load')
            ->once()
            ->with('group')
            ->andReturn($this->getConfigItems());
        $config = new Config($l);
        $this->assertEquals($this->getConfigItems(), $config->get('group'));
    }

    public function testCanSet()
    {
        $l = m::mock('Smorken\Config\LoaderInterface');
        $l->shouldReceive('load')
            ->once()
            ->with('bar')
            ->andReturn($this->getConfigItems());
        $config = new Config($l);
        $config->set('bar.bat', true);
        $this->assertTrue($config->get('bar.bat'));
    }

    protected function getConfigItems()
    {
        return array(
            'foo' => 'bar',
            'baz' => 'bat',
            'bat' => array('fiz' => 'faz'),
        );
    }
} 